'use strict';

/**
 * @ngdoc service
 * @name uiApp.AuthService
 * @description
 * # AuthService
 * Service in the uiApp.
 */
angular.module('uiApp').service('AuthService', function ($window, $q, $http, $rootScope, $cookies) {
	var LOCAL_TOKEN_KEY = 'astitimekeepertoken';
	var LOCAL_USER_INFO = 'atkuser';
	var isAuthenticated = false;
	var authToken;
        $rootScope.showLogout = false;
	$rootScope.roleFlags = {
            isSuperAdmin: false,
            isAdministrator: false,
            isUploader: false,
            isPublic: false
	};

	//load the token saved in local storage
	function loadUserCredentials() {
		var token = $window.localStorage.getItem(LOCAL_TOKEN_KEY);
		if (token) {
			useCredentials(token);
		}
	}

	//store token received from login authentication
	function storeUserCredentials(token) {
		$window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
		useCredentials(token);
	}

	//set user credentials
	function useCredentials(token) {
		// console.log('token:', token);
		if (token != undefined) {
			if ($cookies.get('loginUser')) {
				isAuthenticated = true;
				authToken = token;
				//check if user is admin
				checkRole();
				$http.defaults.headers.common.Authorization = authToken;	
			} else {
				isAuthenticated = false;
			}
			
		} else {
			isAuthenticated = false;
		}
		
	}

	//destroy user sessions
	function destryoUserCredentials() {
		authToken = undefined;
		isAuthenticated = false;
		$rootScope.isAdmin = false;
                $rootScope.showLogout = false;
		$http.defaults.headers.common.Authorization = undefined;
                $rootScope.roleFlags = {
			isSuperAdmin: false,
            isAdministrator: false,
            isViewer: false
		};
		// $window.localStorage.removeItem(LOCAL_TOKEN_KEY);
		$window.localStorage.clear();
	}
        
	//login
	var nativelogin = function (credentials) {
            return $q(function (resolve, reject) {

                $http.post($rootScope.host+'/auth/login/native', credentials)
                    .then(function (result) {
                        if (result.data.success == true) {
                            $cookies.putObject('loginUser',result.data.data);
                            storeUserCredentials(result.data.data.token);
                            checkRole();
                            resolve(true);
                        } else {
                        	// if(result.data.success == false){
                        	// 	$http.post($rootScope.host+'/api/auth/wrongpass', credentials);
                        	// }
                            reject(result.data);  
                        }
                });
            });
        };

    var checkUserStatus = function () {
    	if ($cookies.getObject('loginUser')) {
    		var user = $cookies.getObject('loginUser');
    		return $q(function (resolve, reject) {
    			$http.post($rootScope.host+'/api/auth/status', {username: user.username})
    			.then(
    				function success(result) {
    					resolve(result.data.active);
    				}, 
    				function error(err) {
    					reject(err.data.active);
    				}
				);
    		});
    	} else {
    		return $q(function (resolve, reject) {
    			resolve(false);
    		});
    	}
    };

    //set user roles
    var checkRole = function () {
    	if ($cookies.getObject('loginUser')) {
    		var user = $cookies.getObject('loginUser');
    		if (user.roleFlags) {
    			$rootScope.roleFlags = user.roleFlags;
    			$rootScope.roleFlags.isPublic = true;
    		}
    	}
    };    
        
	//logout
	var logout = function () {
		destryoUserCredentials();
	};
        
	return {
		login: nativelogin,
		logout: logout,
		checkRole: checkRole,
		isAuthenticated: function () { loadUserCredentials(); return isAuthenticated; },
		checkUserStatus: checkUserStatus
	};
})

.factory('AuthInterceptor', function ($rootScope, AUTH_EVENTS, $q) {
	return {
		responseError: function (response) {
			$rootScope.$broadcast({
				401: AUTH_EVENTS.notAuthenticated
			}[response.status], response);

			return $q.reject(response);
		}
	};
})

.config(function ($httpProvider) {
	$httpProvider.interceptors.push('AuthInterceptor');
});
