'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:MemberLoginCtrl
 * @description
 * # MemberLoginCtrl
 * Controller of the uiApp
 */
angular.module('uiApp').controller('MemberLoginCtrl', function ($scope, $rootScope, $state, AuthService,$cookies, $http, $window) {
    $rootScope.setting.layout.pageWithoutHeader = true;
    $rootScope.setting.layout.paceTop = true;
 
    if ($cookies.getObject('loginUser')) {
        // $state.go('app.dashboard.v1'); 
        // $rootScope.setting.layout.pageTopMenu = true;
        $rootScope.showLogout  = true;
        $scope.loginInfo = JSON.parse($cookies.get('loginUser'));
        
        $window.location.href = '/#/app/dashboard/index';
        // $scope.loginUsername = $scope.loginInfo.firstname.toUpperCase();

    }else{
        $rootScope.showLogout  = false;
    }

    if (AuthService.isAuthenticated()) {
        // $scope.loginUsername = $scope.loginInfo.firstname.toUpperCase();
         $state.go('app.dashboard.v1'); 
    }

    $scope.submitForm = function(credentials) {
        console.log(credentials)
        AuthService.login(credentials)
        .then(function (result) {

            //Set menu to visible
            $rootScope.setting.layout.pageTopMenu = true;
            //$('#header').removeClass('navbar-transparent').addClass('navbar-default');

            //Set login to false
            $('.signInBtn').hide();
            $scope.showLogout = true;

            //set user information cookie
            $cookies.put('user',credentials);
            $cookies.putObject('user', credentials);
            
            //Get firstname
            // $scope.loginInfo = JSON.parse($cookies.get('loginUser'));
            // $scope.loginUsername = $scope.loginInfo.firstname.toUpperCase();
            
            //success notification
            $.notify({
                message: 'Successfully Logged In',
                icon: 'fa fa-check-circle-o'
            },{
                // settings
                type: 'success',
                delay: 3000,
                spacing: 10,
                timer: 1000,
                placement: {
                    from: "bottom",
                    align: "right"
                },
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },
                template: '<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">' +
                        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                        '<span data-notify="icon"></span> ' +
                        '<span data-notify="title"><strong>{1}</strong></span> ' +
                        '<span data-notify="message">{2}</span>' +
                        '</div>' 
            });
             $state.go('app.dashboard.v1'); 
            
            //redirect to previous page, otherwise redirect to home
/*
            if ($rootScope.previousPage != '' && $rootScope.previousPage != undefined && $rootScope.previousPage != 'app.dashboard.v1') {
                $state.go($rootScope.previousPage);
            } else {
                $state.go('app.dashboard.v1');     
            }*/
            
        }, function (err) {
            console.log(err.message)
            //Failed Notification
            $.notify({
                // options
                message: err.message,
                icon: 'fa fa-exclamation-circle'
            },
            {
                // settings
                type: 'danger',
                delay: 1000,
                spacing: 10,
                timer: 1000,
                placement: {
                    from: "bottom",
                    align: "right"
                },
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },
                template: '<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">' +
                        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                        '<span data-notify="icon"></span> ' +
                        '<span data-notify="title"><strong>{1}</strong></span> ' +
                        '<span data-notify="message">{2}</span>' +
                        '</div>' 
            });
            //attempt decrement
            // attempt--;
            // $rootScope.socket.emit('blocked', {user: credentials.username});
            // if(attempt <= 0) {
            //     $scope.errorMessage = false;
            // }
        }); 
        // $state.go('app.dashboard.index');

    };
    
    $scope.logout = function(){

        AuthService.logout();
        
        $http.get($rootScope.host+'/auth/logout')
            .then(function (response) {
                
                $scope.showLogout = false;

                // $state.go('app.dashboard.v1');
                $state.go('member.login.index');

                // $rootScope.setting.layout.pageTopMenu = false;

                $.notify({
                    // options
                    //title: 'Success!',
                    message: 'Successfully Logged Out',
                    icon: 'fa fa-check-circle-o'
                },{
                    // settings
                    type: 'success',
                    delay: 3000,
                    spacing: 10,
                    timer: 1000,
                    placement: {
                        from: "bottom",
                        align: "right"
                    },
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    template: '<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">' +
                            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                            '<span data-notify="icon"></span> ' +
                            '<span data-notify="title"><strong>{1}</strong></span> ' +
                            '<span data-notify="message">{2}</span>' +
                            '</div>' 
                });
                


        }, function (err) {
            console.log(err.message);
        });

        var cookies = $cookies.getAll();
        angular.forEach(cookies, function (v, k) {
            $cookies.remove(k);
        });

        $rootScope.credentials = {
            username: '',
            password: ''
        };

        $rootScope.reloadState();

    };

    angular.element(document).ready(function () {
        $('[data-click="change-bg"]').click(function(e) {
            e.preventDefault();
            var targetImage = '[data-id="login-cover-image"]';
            var targetImageSrc = $(this).find('img').attr('src');
            var targetImageHtml = '<img src="'+ targetImageSrc +'" data-id="login-cover-image" />';
        
            $('.login-cover-image').prepend(targetImageHtml);
            $(targetImage).not('[src="'+ targetImageSrc +'"]').fadeOut('slow', function() {
                $(this).remove();
            });
            $('[data-click="change-bg"]').closest('li').removeClass('active');
            $(this).closest('li').addClass('active');	
        });
    });
});
