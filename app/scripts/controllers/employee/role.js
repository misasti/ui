'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:EmployeeRoleCtrl
 * @description
 * # EmployeeRoleCtrl
 * Controller of the uiApp
 */
angular.module('uiApp').controller('EmployeeRoleCtrl', function ($timeout, $scope) {
    angular.element(document).ready(function () {

		/** 		Content Placeholder
   		-----------------------------------------------**/
   		var contentuiT = new Placeload('.placeloadtitle');
   			contentuiT.draw({
	    		width: '350px',
	    		height: '25px',
	    		marginTop: '5px',
	    	});
   		var contentuiB = new Placeload('.placeloadbread');
   			contentuiB.draw({
	    		width: '50px',
	    		height: '10px',
	    		marginTop: '10px',
	    	});
	    	contentuiB.draw({
	    		width: '160px',
	    		height: '10px',
	    		marginTop: '15px',
	    		marginLeft: '10px',
	    		right: true,
	    	});

	    var contentuiL = new Placeload('.placeloadleft');

	    	contentuiL.draw({
	    		width: '200px',
	    		height: '40px',
	    		marginTop: '45px'
	    	});

	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '23px'
	    	});
	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '28px'
	    	});
	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '28px'
	    	});
	    	contentuiL.draw({ //Lastname
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '30px'
	    	});
	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '28px'
	    	});
	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '28px'
	    	});

	    	var contentuiR = new Placeload('.placeloadright');
	    	
	    	contentuiR.draw({
	    		height: '500px',
	    		marginTop: '25px'
	    	});

		    $scope.contentData = false;
		    $scope.contentPlaceholder = false;	

		    var contentHolder = function() {
		    	$scope.contentPlaceholder = true;
		    	$scope.contentData = true;
		    }

	    	$timeout(contentHolder, 1000);

    	/** Coding Starts Here
    	------------------------**/
    	$scope.roleDetail = { 'id': Date.now(), 'name': '', 'permission': ['']}

    	$scope.permissionArray = {
            "User": [
                {
	                "id": 3,
	                "name": "View User"
                },
                {
	                "id": 2,
	                "name": "Update User"
                }
            ],
            "Roles": [
                {
                    "id": 1,
                    "name" : "View Role"
                },
                {
                    "id": 2,
                    "name" : "Delete Role"
                } 
            ]
        };
		

		console.log($scope.permissionArray);


		$scope.items = [1,2,3,4,5];
		$scope.selected = [1];
		$scope.toggle = function (item, list) {
			var idx = list.indexOf(item);
			if (idx > -1) {
				list.splice(idx, 1);
			}
			else {
				list.push(item);
			}
		};
		$scope.exists = function (item, list) {
			return list.indexOf(item) > -1;
		};
		$scope.isIndeterminate = function() {
			return ($scope.selected.length !== 0 &&
				$scope.selected.length !== $scope.items.length);
		};
		$scope.isChecked = function() {
			return $scope.selected.length === $scope.items.length;
		};
		$scope.toggleAll = function() {
			if ($scope.selected.length === $scope.items.length) {
				$scope.selected = [];
			} else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
				$scope.selected = $scope.items.slice(0);
			}
		};


		console.log($scope.items);
    });
});
