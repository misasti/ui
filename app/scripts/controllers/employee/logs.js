'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:EmployeeLogsCtrl
 * @description
 * # EmployeeLogsCtrl
 * Controller of the uiApp
 */
angular.module('uiApp').controller('EmployeeLogsCtrl', 
	function (filterService, DTColumnBuilder, DTOptionsBuilder, 
		DTColumnDefBuilder, $scope, $rootScope, $http, $timeout, $cookies) {
   	angular.element(document).ready(function () {
	
    	/** 			Content Placeholder
   		-----------------------------------------------**/
   		var contentuiT = new Placeload('.placeloadtitle');
   			contentuiT.draw({
	    		width: '350px',
	    		height: '25px',
	    		marginTop: '5px',
	    	});
   		var contentuiB = new Placeload('.placeloadbread');
   			contentuiB.draw({
	    		width: '50px',
	    		height: '10px',
	    		marginTop: '10px',
	    	});
	    	contentuiB.draw({
	    		width: '160px',
	    		height: '10px',
	    		marginTop: '15px',
	    		marginLeft: '10px',
	    		right: true,
	    	});

	    var contentuiL = new Placeload('.placeloadleft');

	    	contentuiL.draw({
	    		width: '200px',
	    		height: '40px',
	    		marginTop: '45px'
	    	});

	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '23px'
	    	});
	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '28px'
	    	});
	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '28px'
	    	});
	    	contentuiL.draw({ //Lastname
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '30px'
	    	});
	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '28px'
	    	});
	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '28px'
	    	});

	    	var contentuiR = new Placeload('.placeloadright');
	    	
	    	contentuiR.draw({
	    		height: '500px',
	    		marginTop: '25px'
	    	});

	    $scope.contentData = false;
	    $scope.contentPlaceholder = false;	

	    var contentHolder = function() {
	    	$scope.contentPlaceholder = true;
	    	$scope.contentData = true;
	    }

    	$timeout(contentHolder, 1000);

    });

	/**		Begin Coding
	------------------------------**/

    /**
     * Display List of Records
     */
   
    $scope.reloadData = reloadData;
    $scope.dtInstance = {};

    function reloadData() {
        var resetPaging = false;
        $scope.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {
        console.log(json);
    }   
	$scope.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle('ID'),
        DTColumnBuilder.newColumn('firstName').withTitle('First name'),
        DTColumnBuilder.newColumn('lastName').withTitle('Last name'),
        DTColumnBuilder.newColumn('timeIn').withTitle('Time-in'),
        DTColumnBuilder.newColumn('timeOut').withTitle('Time-out')
    ];
   
    $scope.dtOptions = DTOptionsBuilder
        .newOptions()
        .withFnServerData(serverData)
        .withDataProp('data') // tried data aswell
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withOption('paging', true) 
        .withOption('bFilter', false) //Search
        .withOption('autoWidth', true)
        .withDisplayLength(10)
        .withPaginationType('simple_numbers')
        .withOption('initComplete', function() {
        	//angular.element('#view-upload thead input').addClass('form-control');
        	angular.element('#view-upload thead input').keydown(function(e){
        		
        		  /*if (event.which == 13) {
				    event.preventDefault();
				    oTable.fnFilter( this.value, $("#view-upload thead input").index(this) );
				  }*/
        	});
        })
        //.withOption('fnRowCallback', rowCallback)
        .withLightColumnFilter({
            '0' : { },
            '1' : { type : 'text' },
            '2' : { type : 'text' },
            '3' : { },
            '4' : { }
        });
        /*function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
	        console.log(aData);
	        return nRow;
	    }*/
       console.log(serverData,'serverData');
    function serverData(sSource, aoData, fnCallback, oSettings) 
    {
    	console.log(sSource,'---sSource');
    	console.log(aoData,'----aoData' );
    	console.log(fnCallback, '---fnCallback');
    	console.log(oSettings, '---oSettings');

        //All the parameters you need is in the aoData variable
        var l  = aoData[4].value;
        var s  = aoData[3].value;
        
        var x = Math.ceil(s/l)+1;

		// console.log('x',x);
		var colsearch   = aoData[1].value;		// search per column

        var draw   = aoData[0].value;             
        var limit  = aoData[4].value;           // item per page
        var order  = aoData[2].value[0].dir;    // order by asc or desc
        var start  = aoData[3].value;           // start from
        var search = aoData[5].value;           // search string
        if(start == 0){
            var start = 1;
        };


        //Then just call your service to get the records from server side
        filterService.execute(start, limit, order, search).then(function(result)
        {    
            console.log(result,'result');
            var data = result.data.data;
            var records = {
                'draw': draw,
                'recordsTotal': result.data.recordsTotal, //Total records, before filtering (i.e. the total number of records in the database)
                'recordsFiltered': result.data.recordsFiltered, //Total records, after filtering (i.e. the total number of records after filtering has been applied - not just the number of records being returned for this page of data).
                'data': data  
            };  

            fnCallback(records);

            // var records = {
            //         'draw': draw,
            //         'recordsTotal': result.data.recordsTotal,
            //         'recordsFiltered': result.data.recordsFiltered,
            //         'data': result.data.es_officers  
            //     };

            // // console.log(records);

            // fnCallback(records);

        }); // end filterService
    } // end serverData


});
