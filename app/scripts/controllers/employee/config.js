'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:EmployeeConfigCtrl
 * @description
 * # EmployeeConfigCtrl
 * Controller of the uiApp
 */
angular.module('uiApp').controller('EmployeeConfigCtrl', function ($timeout, $scope, DTOptionsBuilder, DTColumnDefBuilder) {
	/*$rootScope.setting.layout.pageBgWhite = true;*/
    angular.element(document).ready(function () {

    	/*$('#employee_role').material_select();*/

   		/** 			Content Placeholder
   		-----------------------------------------------**/
   		var contentuiT = new Placeload('.placeloadtitle');
   			contentuiT.draw({
	    		width: '350px',
	    		height: '25px',
	    		marginTop: '5px',
	    	});
   		var contentuiB = new Placeload('.placeloadbread');
   			contentuiB.draw({
	    		width: '50px',
	    		height: '10px',
	    		marginTop: '10px',
	    	});
	    	contentuiB.draw({
	    		width: '160px',
	    		height: '10px',
	    		marginTop: '15px',
	    		marginLeft: '10px',
	    		right: true,
	    	});

	    var contentuiL = new Placeload('.placeloadleft');

	    	contentuiL.draw({
	    		width: '200px',
	    		height: '40px',
	    		marginTop: '45px'
	    	});

	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '23px'
	    	});
	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '28px'
	    	});
	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '28px'
	    	});
	    	contentuiL.draw({ //Lastname
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '30px'
	    	});
	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '28px'
	    	});
	    	contentuiL.draw({
	    		width: '430px',
	    		height: '50px',
	    		marginTop: '28px'
	    	});

	    	var contentuiR = new Placeload('.placeloadright');
	    	
	    	contentuiR.draw({
	    		height: '500px',
	    		marginTop: '25px'
	    	});

	    $scope.contentData = false;
	    $scope.contentPlaceholder = false;	

	    var contentHolder = function() {
	    	$scope.contentPlaceholder = true;
	    	$scope.contentData = true;
	    }

    	$timeout(contentHolder, 1000);

    	

    	/** 		Start Code
    	--------------------------------**/
    	$scope.employee = [
    		{
    			id:1,
    			firstname: "Andrew",
    			lastname: "Ragadio"
    		},
    		{
    			id:21,
    			firstname: "Drew",
    			lastname: "Too"
    		},
    		{
    			id:31,
    			firstname: "Acuhtzi",
    			lastname: "Tzi"
    		},
    		{
    			id:41,
    			firstname: "Onion",
    			lastname: "Quimson"
    		},
    		{
    			id:51,
    			firstname: "Mark",
    			lastname: "Leo"
    		},
    		{
    			id:61,
    			firstname: "Kelly",
    			lastname: "Tubigan"
    		},
    		{
    			id:71,
    			firstname: "Ally",
    			lastname: "Fermante"
    		},
    		{
    			id:81,
    			firstname: "Matet",
    			lastname: "Layosa"
    		},
    		{
    			id:91,
    			firstname: "Qyle",
    			lastname: "John"
    		},
    		{
    			id:101,
    			firstname: "Camille",
    			lastname: "Larios"
    		},
    		{
    			id:102,
    			firstname: "Malou",
    			lastname: "Marie"
    		}
    	];  
    	$scope.employeeDetails = {
    			id: Date.now(),
    			employeeNo: '',
    			username: '',
    			password: '',
    			firstname: '',
    			lastname: '',
    			middlename: '',
    			role: ''

    	};
    	$scope.roles = [
	    	{
	    		id:1,
	    		name: 'Viewer'
	    	},
	    	{
	    		id:2,
	    		name: 'Admin'
	    	}
    	];

    });

});
