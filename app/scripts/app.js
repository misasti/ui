'use strict';

/**
 * @ngdoc overview
 * @name uiApp
 * @description
 * # uiApp
 *
 * Main module of the application.
 */
var app = angular.module('uiApp', 
  [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngMaterial',
    'ui.router',
    'ui.bootstrap',
    'oc.lazyLoad',
    'datatables',
    'datatables.columnfilter',
    'datatables.light-columnfilter'
  ]);

app.config(['$qProvider','$stateProvider', 
        '$urlRouterProvider', '$locationProvider', '$mdThemingProvider', 
    function($qProvider, $stateProvider, $urlRouterProvider, $locationProvider, $mdThemingProvider) {
    $qProvider.errorOnUnhandledRejections(false); // DataTable Fixed Error  https://github.com/angular-ui/ui-router/issues/2889
    $urlRouterProvider.otherwise('/app/dashboard/index');

    $stateProvider
        .state('app', {
            url: '/app',
            templateUrl: 'views/template/app.html',
            abstract: true
        })
        .state('app.dashboard', {
            url: '/dashboard',
            template: '<div ui-view></div>',
            abstract: true
        })
        .state('app.dashboard.index', {
            url: '/index',
            templateUrl: 'views/index.html',
            data: { pageTitle: 'Dashboard' }
        })
        .state('member', {
            url: '/member',
            template: '<div ui-view></div>',
            abstract: true
        })
        .state('member.login', {
            url: '/login',
            template: '<div ui-view></div>',
            abstract: true
        })
        .state('member.login.index', {
            url: '/index',
            data: { pageTitle: 'Login' },
            templateUrl: 'views/employee/login.html'
        })
        .state('app.member', {
            url: '/member',
            template: '<div ui-view></div>',
            abstract: true
        })
        .state('app.member.logs', {
            url: '/logs',
            data: { pageTitle: 'Daily Logs' },
            templateUrl: 'views/employee/logs.html',
            /*resolve: {
                service: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        serie: true,
                        files: [
                            'bower_components/datatables/media/css/dataTables.bootstrap.min.css',
                            'bower_components/datatables/media/js/dataTables.bootstrap.min.js'
                        ]
                    });
                }]
            }*/
        })
        .state('app.employee', {
            url: '/employee',
            template: '<div ui-view></div>',
            abstract: true
        })
        .state('app.employee.config', {
            url: '/config',
            data: { pageTitle: 'Manage User' },
            templateUrl: 'views/employee/config.html'
        })
        .state('app.employee.roles', {
            url: '/roles',
            data: { pageTitle: 'Manage User Roles' },
            templateUrl: 'views/employee/roles.html'
        })
        .state('error', {
            url: '/error',
            data: { pageTitle: '404 Error' },
            templateUrl: 'views/extra_404_error.html'
        });
        $locationProvider.hashPrefix('');

        $mdThemingProvider.theme('default')
        .primaryPalette('teal', { 'default': '700'})
        .accentPalette('orange');
}]);

app.constant('AUTH_EVENTS', {
   notAuthenticated: 'auth-not-authenticated'
});

app.run(['$rootScope', '$state', 'setting', 'AuthService', '$cookies', '$http', '$location', '$window', 
    function($rootScope, $state, setting, AuthService, $cookies, $http, $location, $window) {

    $rootScope.$state = $state;
    $rootScope.setting = setting;

    $rootScope.nodeport = 3000;
    $rootScope.host = $location.protocol() + '://' + 'localhost' + ':' + $rootScope.nodeport;
    $rootScope.credentials = {
        username: '',
        password: ''
    };

    $rootScope.previousPage = "";
        
    $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {
        //check if the status of the user is active
        AuthService.checkUserStatus().then(function(response){ 
            if (next.authenticate && !AuthService.isAuthenticated()) {
                if (next.authenticate == true) {
                    $rootScope.previousPage = fromState.name;
                    event.preventDefault();
                    $state.go('app.dashboard.v1');
                    $rootScope.setting.layout.pageTopMenu = true;
                    $rootScope.setting.layout.pageWithoutSidebar = true;
                    $rootScope.setting.layout.pageBgWhite = true;
                    
                    $.notify({
                        // options
                        message: 'Please login',
                        icon: 'fa fa-exclamation-circle'
                    },
                    {
                        // settings
                        type: 'danger',
                        delay: 1000,
                        spacing: 10,
                        timer: 2000,
                        placement: {
                            from: "bottom",
                            align: "right"
                        },
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        template: '<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">' +
                                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                                '<span data-notify="icon"></span> ' +
                                '<span data-notify="title"><strong>{1}</strong></span> ' +
                                '<span data-notify="message">{2}</span>' +
                                '</div>' 
                    });
                }
            } else {
                if (next.allowedRoles && next.allowedRoles.length > 0) {
                    if (next.superAdminOnly) {
                        if (!$rootScope.roleFlags.isSuperAdmin) {
                            event.preventDefault();
                            $state.go('app.error.401');
                        }
                    } else {
                        var rolesPassed = 0;
                        for (var i=0; i<next.allowedRoles.length; i++) {
                            if ($rootScope.roleFlags[next.allowedRoles[i]] == true) {
                                rolesPassed++;
                            }
                        }
                        
                        if (rolesPassed == 0) {
                            event.preventDefault();
                            $state.go('app.error.401');
                        }
                    }
                }
            }
        }); 
    });

     //$rootScope global functions
    $rootScope.reloadState = function () {
        $state.go($state.current, {}, {reload:true});
    };
    

}]);
